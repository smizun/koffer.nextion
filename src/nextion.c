#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <time.h>   // time calls
#include <math.h>

#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <sys/wait.h>
//
#include <netinet/in.h>
#include <linux/rtnetlink.h>
#include <memory.h>
#include <arpa/inet.h>


#include <net/if.h>

#include "swlib.h"

/* data types */
enum switch_val_type {
	SWITCH_TYPE_UNSPEC,
	SWITCH_TYPE_INT,
	SWITCH_TYPE_STRING,
	SWITCH_TYPE_PORTS,
	SWITCH_TYPE_LINK,
	SWITCH_TYPE_NOVAL,
};

#define PIC_VPN_OFF 1
#define PIC_VPN_ERROR 2
#define PIC_VPN_ON  3
#define PIC_ALARM_OFF 4
#define PIC_ALARM_ERROR 5
#define PIC_ALARM_ON  6
#define PIC_VIDEO_OFF 7
#define PIC_VIDEO_ERROR 8
#define PIC_VIDEO_ON  9
#define PIC_LAN_OFF 10
#define PIC_LAN_ERROR 11
#define PIC_LAN_ON  12
#define PIC_WLAN_OFF  13
#define PIC_WLAN_ERROR  14
#define PIC_WLAN_ON 15
#define PIC_MOBILE_OFF  16
#define PIC_MOBILE_ERROR  17
#define PIC_3G_ON 18
#define PIC_LTE_ON  19
#define PIC_SETUP_OFF  20
#define PIC_SETUP_ERROR  21
#define PIC_SETUP_ON 22
#define PIC_WAN_OFF  23
#define PIC_WAN_ERROR  24
#define PIC_WAN_ON 25

#define PIC_VPN_OFF_SLAVE 26
#define PIC_VPN_ON_SLAVE 27
#define PIC_VPN_ERROR_SLAVE 28
#define PIC_ALARM_OFF_SLAVE 29
#define PIC_ALARM_ON_SLAVE 30
#define PIC_ALARM_ERROR_SLAVE 31
#define PIC_INFO_OFF_SLAVE 32
#define PIC_INFO_ON_SLAVE 33
#define PIC_INFO_ERROR_SLAVE 34
#define PIC_LAN_OFF_SLAVE 35
#define PIC_LAN_ON_SLAVE 36
#define PIC_LAN_ERROR_SLAVE 37
#define PIC_MOBILE_OFF_SLAVE  38
#define PIC_LTE_SLAVE  39
#define PIC_3G__SLAVE 40
#define PIC_MOBILE_ERROR_SLAVE  41
#define PIC_VIDEO_OFF_SLAVE 42
#define PIC_VIDEO_ON_SLAVE 43
#define PIC_VIDEO_ERROR_SLAVE 44
#define PIC_WAN_OFF_SLAVE 45
#define PIC_WAN_ON_SLAVE 46
#define PIC_WAN_ERROR_SLAVE 47
#define PIC_WLAN_OFF_SLAVE 48
#define PIC_WLAN_ON_SLAVE 49
#define PIC_WLAN_ERROR_SLAVE 50

#define DATASZ 16 
#define MODEMASK ( S_IRUSR | S_IRGRP | S_IROTH )

#define IND_VPN       0
#define IND_WAN       1
#define IND_LAN       2
#define IND_MODEM     3
#define IND_WLAN      4

#define IMG_VPN 0
#define IMG_ALARM 1
#define IMG_VIDEO 2
#define IMG_INFO 3
#define IMG_WAN 4
#define IMG_MODEM 5
#define IMG_WLAN 6
#define IMG_LAN 7

#define INT_UP 1
#define INT_DOWN 0

#define INT_IP 1
#define INT_NOIP 0

#define PORT_OK 0
#define PORT_CLOSE -1

#define STATUS_RED 1
#define STATUS_GREEN 2
#define STATUS_BLUE 3
#define STATUS_BLACK 4

struct {
	char name[64];
	char rxstr[64];            /* Received packet count. */
	char txstr[64];            /* Transmitted packet count. */
	char ipstr[64];
        int updown;     // 0 - down, 1 - up
        int ipadr;      // 0 - no ip, 1 - ip
} netz[5];
	

struct {
	char name[32];
	char ip[32];
	int port;
        int status;      // -1 - no connect, 0 - connect
        char statusstr[8];
} video[5];

struct modem {
        char oper[64];
	char mode[64];
	char signal[64];
	char network[64];
	int lte;
} modem;

struct {
    int status;
    int oldstatus;
} imagestatus[8];

int isAlarm;
int isVideo;
int fd;
int fdmonitor;
struct sockaddr_nl    local;    // локальный адрес
char bufmonitor[8192];                // буфер сообщения
struct iovec iov;            // структура сообщения
struct msghdr msg; 

int parse_gsm();

int parse_gsm()
{
    char *filePath = NULL;      /* Path to /proc/net/dev file. */
    FILE *file = NULL;        /* File pointer for fopen(). */
    int status = 0;       /* /proc/net/dev file close() status. */


    printf( "parse_gsm\n");
    filePath = "/tmp/log.txt";

    /*
     * Open /proc/net/dev for reading.
     */
    if( ( file = fopen( filePath, "r" ) ) == NULL )
    {
        return (-1);
    }

    fgets( modem.oper, 64, file );
    modem.oper[ strlen( modem.oper)-1]= 0;
    fgets( modem.signal, 64, file );
    modem.signal[ strlen( modem.signal)-1]= 0;
    fgets( modem.mode, 64, file );
    modem.mode[ strlen( modem.mode)-1]= 0;
    fgets( modem.network, 64, file );
    modem.network[ strlen( modem.network)-1]= 0;
    
    if ( strstr( modem.mode, "LTE") != NULL)
      modem.lte = 2;
    else 
      modem.lte = 1;
      

    /*
     * Clean-up via close().
     */
    if( ( status = fclose( file ) != 0 ) )
    {
        return (-1);
    }
    printf( "parse_gsm OK\n");

    return (0);
}

/* Utility function for parse rtattr. */
static void 
netlink_parse_rtattr (struct rtattr **tb, int max, struct rtattr *rta, int len)
{
    while (RTA_OK (rta, len))
    {
	if (rta->rta_type <= max)
	    tb[rta->rta_type] = rta;
	rta = RTA_NEXT (rta, len);
    }
}

/* Recieving netlink message. */
void netlink_recv(int nsock) {
    char buf[4096];
    struct iovec iov = { buf, sizeof(buf) };
    struct sockaddr_nl snl;
    struct msghdr msg = { (void*)&snl, sizeof(snl), &iov, 1, NULL, 0, 0};
    struct nlmsghdr *h;
    struct rtattr *tb[IFLA_MAX > IFA_MAX ? IFLA_MAX : IFA_MAX + 1];
    struct ifinfomsg *ifi;
    struct nlmsgerr *err;

    int status = recvmsg(nsock, &msg, 0);

    if(status < 0) {
	perror("recvmsg(nsock)");
	return;
    } else if (status == 0) {
	perror("recvmsg(nsock): received zero sizenetlink message");
	return;
    }

    if(msg.msg_namelen != sizeof(snl)){
	perror("recvmsg(nsock): received invalid netlink message");
	return;
    }

    if(msg.msg_flags & MSG_TRUNC){
	perror("recvmsg(nsock): received truncated netlink message");
	return;
    }

    for(h = (struct nlmsghdr *) buf; NLMSG_OK(h, status);
	h = NLMSG_NEXT (h, status)) {

	switch(h->nlmsg_type) {
	case NLMSG_DONE:
	    return;
	case NLMSG_ERROR:
	    err = (struct nlmsgerr *) NLMSG_DATA(h);
	    if (h->nlmsg_len < NLMSG_LENGTH(sizeof(struct nlmsgerr))) {
		perror("received error message with invalid length");
		return;
	    }
	    return;
	case RTM_NEWLINK:
	case RTM_DELLINK:
            ifi = NLMSG_DATA(h);;

	    int len = h->nlmsg_len - NLMSG_LENGTH(sizeof(struct ifinfomsg));
	    char ifname[IF_NAMESIZE+1];
	    if(len < 0)
		continue;

            memset(tb, 0, sizeof(tb));
	    netlink_parse_rtattr (tb, IFLA_MAX, IFLA_RTA (ifi), len);

	    if(tb[IFLA_IFNAME] == NULL)
		continue;

	    strncpy(ifname, (char *) RTA_DATA (tb[IFLA_IFNAME]), IF_NAMESIZE);

		int index = -1;
		index= getIndex( ifname);
                if ( index > -1)
		{
			switch (h->nlmsg_type) {
			case RTM_NEWLINK:
			if(ifi->ifi_flags & IFF_RUNNING)
			{
			  printf("interface %s(%d): UP\n", ifname, index);
                          netz[index].updown = INT_UP;
			} 
			else 
			{
			  printf("interface %s(%d): DOWN\n", ifname, index);
                          netz[index].updown = INT_DOWN;
			}
			break;
			case RTM_DELLINK:
			  printf("interface %s(%d): REMOVED\n",  ifname, index);
                          netz[index].updown = INT_DOWN;
			break;
			}
		}
            break;
	default:
	    continue;
	}
    }
}

/* Make the kernel send us an RTM_NEWLINK for all interfaces */
void netlink_getlink(int nsock) {
    struct nlmsghdr* n;
    struct ifinfomsg *ifi;
    u_int8_t req[sizeof(struct nlmsghdr) + sizeof(struct ifinfomsg) +
		 sizeof(struct ifaddrmsg) + 4096];

    memset(&req, 0, sizeof(req));
    n = (struct nlmsghdr*) req;
    n->nlmsg_len = NLMSG_LENGTH(sizeof(*ifi));
    n->nlmsg_type = RTM_GETLINK;
    n->nlmsg_seq = 1;
    n->nlmsg_flags = NLM_F_ROOT | NLM_F_MATCH | NLM_F_REQUEST;
    n->nlmsg_pid = 0;

    ifi = NLMSG_DATA(n);
    ifi->ifi_family = AF_UNSPEC;
    ifi->ifi_change = -1;

    if (send(nsock, n, n->nlmsg_len, 0) < 0) {
		perror("send");
	exit(1);
    }    
}


int checkInterfaces() {
    struct sockaddr_nl nl_addr;
    int nsock,ret;
    fd_set r;
	struct timeval timeout;      
	
    timeout.tv_sec = 0;
    timeout.tv_usec = 500000;
       

    /* initialize netlink socket */
    if((nsock = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0) {
		perror("socket");
		return 2;
    }
 
    
    memset(&nl_addr, 0, sizeof(nl_addr));
    nl_addr.nl_family = AF_NETLINK;
    nl_addr.nl_groups = RTMGRP_LINK;
    nl_addr.nl_pid = getpid();

    if((bind(nsock, (struct sockaddr *) &nl_addr, sizeof(nl_addr))) < 0 ) {
		perror("bind");
		return 2;
    }

    netlink_getlink(nsock);
    netlink_recv(nsock);

	int i= 0;
    while(i< 5) {
		FD_ZERO(&r);
		FD_SET(nsock, &r);

		ret = select(nsock + 1, &r, NULL, NULL, &timeout);
		switch (ret) {
		case -1:
			if (errno != EINTR) {
			perror("select");
			return 2;
			}
			continue;
		case 0:	 /* timeout */
			i= 10;
			break;
		default:
			break;
		}

		if (FD_ISSET(nsock, &r))
			netlink_recv(nsock);
	    i++;
    }
    close(nsock);
    return 0;
}



// небольшая вспомогательная функция, которая с помощью макрасов netlink разбирает сообщение и
// помещает блоки данных в массив атрибутов rtattr
void parseRtattr(struct rtattr *tb[], int max, struct rtattr *rta, int len)
{
    memset(tb, 0, sizeof(struct rtattr *) * (max + 1));

    while (RTA_OK(rta, len)) {    // пока сообщение не закончилось
        if (rta->rta_type <= max) {
            tb[rta->rta_type] = rta;    //читаем атрибут
        }
        rta = RTA_NEXT(rta,len);    // получаем следующий атрибут
    }
}

int loadConfig()
{
    char *filePath = NULL;      /* Path to /proc/net/dev file. */
    FILE *file = NULL;        /* File pointer for fopen(). */
    int status = 0;       /* /proc/net/dev file close() status. */
    char buf[1024] = "";      /* Character buffer. */
    int conv = 0;       /* sscanf() string match count. */
    int index;

    filePath = "/etc/nextion.conf";

    /*
     * Open /proc/net/dev for reading.
     */
    if( ( file = fopen( filePath, "r" ) ) == NULL )
    {
        perror( "fopen" );
        return (-1);
     }
	int i= 0;
    while( fgets( buf, sizeof( buf ), file ) )
    {
		conv = sscanf( buf, "%s %s %u",
                 &video[i].name, &video[i].ip,  &video[i].port );
        if ( ++i > 4)
			break;
                 
    }
    /*
     * Clean-up via close().
     */
    if( ( status = fclose( file ) != 0 ) )
    {
        perror( "fclose" );
        return (-1);
     }
/*
    for ( i=0; i< 5; i++)
    {
        printf("i=%d %s %s %d\n", i,  video[i].name, video[i].ip,  video[i].port);    
    }
*/
    return (0);
}
int getVideoStatus()
{
	int i= 0;
	int err = 0;
	
	for ( i=0; i< 5; i++)
	{	
		if ( video[i].port > 0)
		{
                        video[i].status= checkVideo( video[i].ip, video[i].port);
                        if ( video[i].status == PORT_OK)
                            video[i].statusstr = "OK";
                        else
                            video[i].statusstr = "FEHLER";

			err += video[i].status;
		}
	}
	return err;
}


int getIndex( char *name)
{
  int index = -1;

  if( ( strcmp( name, "tap0" ) ) == 0 )
    index = IND_VPN;
  if( ( strcmp( name, "eth1" ) ) == 0 )
    index = IND_WAN;
  if( ( strcmp( name, "br-lan" ) ) == 0 )
    index = IND_LAN;
  if( ( strcmp( name, "eth2" ) ) == 0 )
    index = IND_MODEM;
  if( ( strcmp( name, "wlan0" ) ) == 0 )
    index = IND_WLAN; 

  return index; 

}
void getIp(char *if_name)
{

  printf( "getIp %s\n", if_name);

  struct ifreq ifr;
  size_t if_name_len=strlen(if_name);
  if (if_name_len<sizeof(ifr.ifr_name)) {
      memcpy(ifr.ifr_name,if_name,if_name_len);
      ifr.ifr_name[if_name_len]=0;
  } else {
      printf("interface name is too long");
  }
  int fd=socket(AF_INET,SOCK_DGRAM,0);
  if (fd==-1) {
      printf("%s",strerror(errno));
  }
  int index = getIndex ( if_name);
  if ( index > -1)
  {
    if (ioctl(fd,SIOCGIFADDR,&ifr)==-1) {
        int temp_errno=errno;
        close(fd);
        sprintf( netz[index].ipstr, "___.___.___.___");
        netz[index].ipadr = INT_NOIP;
    }
    else
    {
      close(fd);  
      struct sockaddr_in* ipaddr = (struct sockaddr_in*)&ifr.ifr_addr;
      sprintf( netz[index].ipstr, "IP: %s",inet_ntoa(ipaddr->sin_addr));  
      printf( "IP: %s",inet_ntoa(ipaddr->sin_addr));  
      netz[index].ipadr = INT_IP;
    }
  }
  printf( "getIp %s OK\n", if_name);
}

int ifMetric()
{
    struct stat fileAttrs;      /* /proc/net/dev file attributes. */
    struct stat linkAttrs;      /* /proc/net/dev symlink attributes. */
    char *filePath = NULL;      /* Path to /proc/net/dev file. */
    FILE *file = NULL;        /* File pointer for fopen(). */
    int status = 0;       /* /proc/net/dev file close() status. */
    char buf[1024] = "";      /* Character buffer. */
    struct ifList *cur = NULL;      /* Current node in linked list. */
    int conv = 0;       /* sscanf() string match count. */
    long long rxPackets = 0;            /* Received packet count. */
    long long txPackets = 0;            /* Transmitted packet count. */
    long long rxb = 0L;            /* Received packet count. */
    long long txb = 0L;            /* Transmitted packet count. */
    char *delim = NULL;       /* Ptr to matched character in string. */
    unsigned int ifIndex = 0;     /* Interface index. */
    mode_t mode = 0;        /* /proc/net/dev file permissions. */
    int index;
    int i;


    printf( "ifMetric\n");

    filePath = "/proc/net/dev";

    /*
     * Get /proc/net/dev symlink attributes.
     */
    if( ( lstat( filePath, &linkAttrs ) ) != 0 )
    {
        perror( "lstat" );
        return (-1);
     }

    /*
     * Is /proc/net/dev a symlink?
     */
    if( S_ISLNK( linkAttrs.st_mode ) )
    {
        fprintf( stderr, "ifchk: ERROR: /proc/net/dev is a symbolic link\n" );
        return (-1);
     }

    /*
     * Open /proc/net/dev for reading.
     */
    if( ( file = fopen( "/proc/net/dev", "r" ) ) == NULL )
    {
        perror( "fopen" );
        return (-1);
     }

    /*
     * Get /proc/net/dev file attributes.
     */
    if( ( fstat( fileno( file ), &fileAttrs ) ) != 0 )
    {
        perror( "fstat" );
        return (-1);
     }

    /*
     * Does lstat(2) and fstat(2) agree on /proc/net/dev?
     */
    if( ( ( linkAttrs.st_ino ) != ( fileAttrs.st_ino ) ) || 
        ( ( linkAttrs.st_dev ) != ( fileAttrs.st_dev ) ) )
    {
        fprintf( stderr, "ifchk: ERROR: /proc/net/dev file attribute inconsistency\n" );
        return (-1);
     }

    /*
     * Is /proc/net/dev a regular file?
     */
    if( ! ( S_ISREG( fileAttrs.st_mode ) ) )
    {
        fprintf( stderr, "ifchk: ERROR: /proc/net/dev is not a regular file\n" );
        return (-1);
     }

    /*
     * Is /proc/net/dev zero bytes in length?
     */
    if( ( ( fileAttrs.st_size ) || ( fileAttrs.st_blocks ) ) != 0 )
    {
        fprintf( stderr, "ifchk: ERROR: /proc/net/dev file size is greater than 0\n" );
        return (-1);
     }

    /*
     * Is /proc/net/dev chowned UID/GID 0?
     */
    if( ( ( fileAttrs.st_uid ) || ( fileAttrs.st_gid ) ) != 0 )
    {
        fprintf( stderr, "ifchk: ERROR: /proc/net/dev is not owned by UID 0, GID 0\n" );
        return (-1);
     }

    /*
     * Is /proc/net/dev mode 0444?
     */
    if( ( mode = fileAttrs.st_mode & ALLPERMS ) != MODEMASK )
    {
        fprintf( stderr, "ifchk: ERROR: /proc/net/dev permissions are not mode 0444\n" );
        return (-1);
     }

    /*
     * Read past 1st and 2nd line of /proc/net/dev header.
     */
    if( ! fgets(buf, sizeof( buf ), file) )
    {
        perror( "fgets" );
        return (-1);
     }

    if( ! fgets(buf, sizeof( buf ), file) )
    {
        perror( "fgets" );
        return (-1);
     }

    /*
     * Check /proc/net/dev format.
     */
    if( ( strstr( buf, "compressed" ) ) == NULL )
    {
        fprintf( stderr, "ifchk: ERROR: /proc/net/dev header format is not supported\n" );
        return (-1);
     }

    /*
     * Prepare to display metrics data.
     */

    /*
     * Loop through /proc/net/dev metrics & output
     * one line of metrics data for each interface.
     */
    while( fgets( buf, sizeof( buf ), file ) )
    {
        /*
         * Is this the current interface?
         */
        int index = -1 ; //getIndex(buf);
        
        if( ( strstr( buf, "tap0" ) ) != NULL )
            index = IND_VPN;
        if( ( strstr( buf, "eth1" ) ) != NULL )
            index = IND_WAN;
        if( ( strstr( buf, "br-lan" ) ) != NULL )
            index = IND_LAN;
        if( ( strstr( buf, "eth2" ) ) != NULL )
            index = IND_MODEM;
        if( ( strstr( buf, "wlan0" ) ) != NULL )
            index = IND_WLAN;


        if( index > -1 )
        {
            delim = strchr( buf, ':' );

            /*
             * Select & process the correct output format string.
             */
            if( *( delim + 1 ) == ' ' )
            {
                conv = sscanf( buf,
                "%*s %Lu %Lu %*lu %*lu %*lu %*lu %*lu %*lu %Lu %Lu %*lu %*lu %*lu %*lu %*lu %*lu",
                 &rxb, &rxPackets,  &txb, &txPackets );
            }

            else
            {
                conv = sscanf( buf,
                "%*s %Lu %*lu %*lu %*lu %*lu %*lu %*lu %Lu %Lu %*lu %*lu %*lu %*lu %*lu %*lu",
                  &rxPackets, &txb, &txPackets );
            }

            if ( rxb < 1024L)
                sprintf(netz[index].rxstr, "Received: %Lu bytes", rxb);
            else if ( rxb < 0x100000L)  // 1Mb
	    {
                sprintf(netz[index].rxstr, "Received: %Lu bytes (%Lu Kb)", rxb, rxb/1024L);
	    }
            else if ( rxb < 0x40000000L)  //1 Gb
            {    
                sprintf(netz[index].rxstr, "Received: %Lu bytes (%Lu Mb)", rxb, rxb/0x100000L);
            }
            else 
 	    {
                sprintf(netz[index].rxstr, "Received: %Lu bytes (%Lu Gb)", rxb, rxb/0x40000000L);
	    }
            //printf("%s", rxstr[index]);    

            if ( txb < 1024L)
                sprintf(netz[index].txstr, "Sended: %Lu bytes", txb);
            else if ( txb < 0x100000L)  // 1Mb
                sprintf(netz[index].txstr, "Sended: %Lu bytes (%Lu Kb)", txb, txb/1024L);
            else if ( txb < 0x40000000L)  //1 Gb
                sprintf(netz[index].txstr, "Sended: %Lu bytes (%Lu Mb)", txb, txb/0x100000L);
            else 
                sprintf(netz[index].txstr, "Sended: %Lu bytes (%Lu Gb)", txb, txb/0x40000000L);
            //printf("%s", txstr[index]);    

         }
     }

    /*
     * Clean-up via close().
     */
    if( ( status = fclose( file ) != 0 ) )
    {
        perror( "fclose" );
        return (-1);
     }
/*
    for ( i=0; i< 5; i++)
    {
        printf("i=%d %s", i,  rxstr[i]);    
        printf("i=%d %s", i, txstr[i]);    

    }
*/
printf( "ifMetric OK\n");

    return (0);
}
    

int open_port(void)
{
    int fd; // file description for the serial port
    fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);
    if(fd == -1) // if open is unsucessful
    {
        printf("open_port: Unable to open /dev/ttyATH0\n");
    }
    else
    {
        printf("port is open %d\n", fd);
    }

    return(fd);
} //open_port

int configure_port(int fd)      // configure the port
{
  struct termios port_settings;      // structure to store the port settings in

  tcgetattr(fd,&port_settings);
  cfsetispeed(&port_settings, B9600);
  cfsetospeed(&port_settings, B9600);

  port_settings.c_cflag &= ~PARENB;
  port_settings.c_cflag &= ~CSTOPB;
  port_settings.c_cflag &= ~CSIZE;
  port_settings.c_cflag |= CS8;

  tcflush(fd,TCIFLUSH);
  tcsetattr(fd, TCSANOW, &port_settings);    // apply the settings to the port
  return(fd);

} //configure_port

void sendCommand( char *cmd)
{
  char buf[1024];
  unsigned char end[3]= {0xff, 0xff, 0xff};

  if ( fd > -1)
  {
    //usleep(100000);
    int rd = read ( fd, buf, 1024);
    while ( rd > 0)
    {
      rd = read ( fd, buf, 1024);
      //printf( "Read=%d\n", rd);
    }
    printf("[%s]\n", cmd);
    int status = write(fd, (void *)cmd, strlen(cmd));
    status += write(fd, (void *)end, 3);  
    //printf( "Status=%d\n", status);
    usleep(400000);
  }
}

void updateStatus()
{
    if ( imagestatus[IMG_VPN].oldstatus != imagestatus[IMG_VPN].status)
    {
        setVPN( imagestatus[IMG_VPN].status);
        imagestatus[IMG_VPN].oldstatus = imagestatus[IMG_VPN].status;
    }
    if ( imagestatus[IMG_ALARM].oldstatus != imagestatus[IMG_ALARM].status)
    {
        setAlarm(imagestatus[IMG_ALARM].status);
        imagestatus[IMG_ALARM].oldstatus = imagestatus[IMG_ALARM].status;
    }
    if ( imagestatus[IMG_VIDEO].oldstatus != imagestatus[IMG_VIDEO].status)
    {
        setVideo(imagestatus[IMG_VIDEO].status);
        imagestatus[IMG_VIDEO].oldstatus = imagestatus[IMG_VIDEO].status;
    }
    if ( imagestatus[IMG_INFO].oldstatus != imagestatus[IMG_INFO].status)
    {
        setInfo(imagestatus[IMG_INFO].status);
        imagestatus[IMG_INFO].oldstatus = imagestatus[IMG_INFO].status;
    }
    if ( imagestatus[IMG_WAN].oldstatus != imagestatus[IMG_WAN].status)
    {
        setWAN(imagestatus[IMG_WAN].status);
        imagestatus[IMG_WAN].oldstatus = imagestatus[IMG_WAN].status;
    }
    if ( imagestatus[IMG_MODEM].oldstatus != imagestatus[IMG_MODEM].status)
    {
        setModem(imagestatus[IMG_MODEM].status);
        imagestatus[IMG_MODEM].oldstatus = imagestatus[IMG_MODEM].status;
    }
    if ( imagestatus[IMG_WLAN].oldstatus != imagestatus[IMG_WLAN].status)
    {
        setWLAN(imagestatus[IMG_WLAN].status);
        imagestatus[IMG_WLAN].oldstatus = imagestatus[IMG_WLAN].status;
    }
    if ( imagestatus[IMG_LAN].oldstatus != imagestatus[IMG_LAN].status)
    {
        setLan(imagestatus[IMG_LAN].status);
        imagestatus[IMG_LAN].oldstatus = imagestatus[IMG_LAN].status;
    }

    /*
	if ( netz[IND_VPN].oldstatus != netz[IND_VPN].status)
	{
	  setVPN( netz[IND_VPN].status);
	  netz[IND_VPN].oldstatus = netz[IND_VPN].status;
	}

	if ( netz[IND_WAN].oldstatus != netz[IND_WAN].status)
	{
	  setWAN( netz[IND_WAN].status);
	  netz[IND_WAN].oldstatus = netz[IND_WAN].status;
	}
	printf("LTE=%d\n", modem.lte);
	setModem( modem.lte);
	setAlarm( isAlarm);
        */
}

void setLan( int mode )
{
  char cmd[64];
  
  
  if ( mode == STATUS_BLACK)
  {
    sprintf(cmd, "page0.valan.val=%d", PIC_LAN_OFF);
    sendCommand(cmd); 
    sprintf(cmd, "page0.valanslave.val=%d", PIC_LAN_OFF_SLAVE);
    sendCommand(cmd);
    //sprintf(cmd, "plan.pic=%d", PIC_LAN_OFF);
    //sendCommand(cmd);
    //sprintf(cmd, "pagelan.plan2.pic=%d", PIC_LAN_OFF_SLAVE);
    //sendCommand(cmd);
  }	
  if ( mode == STATUS_GREEN)
  {
	sprintf(cmd, "page0.valan.val=%d", PIC_LAN_ON); 
    sendCommand(cmd); 
	sprintf(cmd, "page0.valanslave.val=%d", PIC_LAN_ON_SLAVE); 
    sendCommand(cmd); 

    //sprintf(cmd, "plan.pic=%d", PIC_LAN_ON);
    //sendCommand(cmd);
//	sprintf(cmd, "pagelan.plan2.pic=%d", PIC_LAN_ON_SLAVE);
    //sendCommand(cmd);
  }	
  if ( mode == STATUS_RED)
  {
	sprintf(cmd, "page0.valan.val=%d", PIC_LAN_ERROR); 
    sendCommand(cmd); 
	sprintf(cmd, "page0.valanslave.val=%d", PIC_LAN_ERROR_SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "plan.pic=%d", PIC_LAN_ERROR);
    sendCommand(cmd); 
    sprintf(cmd, "pagelan.plan2.pic=%d", PIC_LAN_ERROR_SLAVE);
    sendCommand(cmd); 
    */
  }	
  //sendCommand("ref plan.pic");
  //sendCommand("ref pagelan.plan2");
  
}
void setWAN( int mode )
{
  char cmd[128];
  if ( mode == STATUS_BLACK)
  {
	sprintf(cmd, "page0.vawan.val=%d", PIC_WAN_OFF); 
	sendCommand(cmd); 
	sprintf(cmd, "page0.vawanslave.val=%d", PIC_WAN_OFF_SLAVE); 
	sendCommand(cmd); 
        /*
	sprintf(cmd, "pwan.pic=%d", PIC_WAN_OFF); 
	sendCommand(cmd); 
	sprintf(cmd, "pagewan.pwan2.pic=%d", PIC_WAN_OFF_SLAVE); 
	sendCommand(cmd); 
        */
  }
  if ( mode == STATUS_GREEN)
  {
    sprintf(cmd, "page0.vawan.val=%d", PIC_WAN_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "page0.vawanslave.val=%d", PIC_WAN_ON_SLAVE);  
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pwan.pic=%d", PIC_WAN_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "pagewan.pwan2.pic=%d", PIC_WAN_ON_SLAVE);  
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_RED)
  {
	sprintf(cmd, "page0.vawan.val=%d", PIC_WAN_ERROR); 
	sendCommand(cmd); 
	sprintf(cmd, "page0.vawanslave.val=%d", PIC_WAN_ERROR_SLAVE); 
	sendCommand(cmd); 
        /*
	sprintf(cmd, "pwan.pic=%d", PIC_WAN_ERROR); 
	sendCommand(cmd); 
	sprintf(cmd, "pagewan.pwan2.pic=%d", PIC_WAN_ERROR_SLAVE); 
	sendCommand(cmd); 
        */
  }
 
  //sendCommand("ref pwan.pic");
  //sendCommand("ref pagewan.pwan2");
  
}
void setInfo( int mode )
{
  char cmd[64];
  if ( mode == STATUS_BLACK)
  {
    sprintf(cmd, "page0.vasetup.val=%d", PIC_SETUP_OFF);
    sendCommand(cmd); 
    sprintf(cmd, "page0.vasetupslave.val=%d", PIC_INFO_OFF_SLAVE);
    sendCommand(cmd); 
    /*
    sprintf(cmd, "psetup.pic=%d", PIC_SETUP_OFF); 
    sendCommand(cmd); 
    sprintf(cmd, "pagesetup.psetup2.pic=%d", PIC_INFO_OFF_SLAVE); 
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_GREEN)
  {
    sprintf(cmd, "page0.vasetup.val=%d", PIC_SETUP_ON);
    sendCommand(cmd); 
    sprintf(cmd, "page0.vasetupslave.val=%d", PIC_INFO_ON_SLAVE);
    sendCommand(cmd); 
    /*
    sprintf(cmd, "psetup.pic=%d", PIC_SETUP_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "pagesetup.psetup2.pic=%d", PIC_INFO_ON_SLAVE);  
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_RED)
  {
    sprintf(cmd, "page0.vasetup.val=%d", PIC_SETUP_ERROR);
    sendCommand(cmd); 
    sprintf(cmd, "page0.vasetupslave.val=%d", PIC_INFO_ERROR_SLAVE);
    sendCommand(cmd); 
    /*
    sprintf(cmd, "psetup.pic=%d", PIC_SETUP_ERROR); 
    sendCommand(cmd); 
    sprintf(cmd, "pagesetup.psetup2.pic=%d", PIC_INFO_ERROR_SLAVE); 
    sendCommand(cmd); 
    */
  }
  /*
  sendCommand("ref psetup.pic"); 
  sendCommand("ref pagesetup.psetup2");
  */
}
void setVPN( int mode )
{
  char cmd[64];
  if ( mode == STATUS_BLACK)
  {
    sprintf(cmd, "page0.vavpn.val=%d", PIC_VPN_OFF); 
    sendCommand(cmd); 
    sprintf(cmd, "page0.vavpnslave.val=%d", PIC_VPN_OFF_SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pvpn.pic=%d", PIC_VPN_OFF); 
    sendCommand(cmd); 
    sprintf(cmd, "pagevpn.pvpn2.pic=%d", PIC_VPN_OFF_SLAVE); 
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_GREEN)
  {
    sprintf(cmd, "page0.vavpn.val=%d", PIC_VPN_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "page0.vavpnslave.val=%d", PIC_VPN_ON_SLAVE);  
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pvpn.pic=%d", PIC_VPN_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "pagevpn.pvpn2.pic=%d", PIC_VPN_ON_SLAVE);  
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_RED)
  {
    sprintf(cmd, "page0.vavpn.val=%d", PIC_VPN_ERROR); 
    sendCommand(cmd); 
    sprintf(cmd, "page0.vavpnslave.val=%d", PIC_VPN_ERROR_SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pvpn.pic=%d", PIC_VPN_ERROR); 
    sendCommand(cmd); 
    sprintf(cmd, "pagevpn.pvpn2.pic=%d", PIC_VPN_ERROR_SLAVE); 
    sendCommand(cmd); 
    */
  }
  //sendCommand("ref pvpn.pic");
  //sendCommand("ref pagevpn.pvpn2");
}
void setWLAN( int mode )
{
  char cmd[64];
  if ( mode == STATUS_BLACK)
  {
    sprintf(cmd, "page0.vawlan.val=%d", PIC_WLAN_OFF); 
    sendCommand(cmd); 
    sprintf(cmd, "page0.vawlanslave.val=%d", PIC_WLAN_OFF_SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pwlan.pic=%d", PIC_WLAN_OFF); 
    sendCommand(cmd); 
    sprintf(cmd, "pagewlan.pwlan2.pic=%d", PIC_WLAN_OFF_SLAVE); 
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_GREEN)
  {
    sprintf(cmd, "page0.vawlan.val=%d", PIC_WLAN_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "page0.vawlanslave.val=%d", PIC_WLAN_ON_SLAVE);  
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pwlan.pic=%d", PIC_WLAN_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "pagewlan.pwlan2.pic=%d", PIC_WLAN_ON_SLAVE);  
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_RED)
  {
    sprintf(cmd, "page0.vawlan.val=%d", PIC_WLAN_ERROR); 
    sendCommand(cmd); 
    sprintf(cmd, "page0.vawlanslave.val=%d", PIC_WLAN_ERROR_SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pwlan.pic=%d", PIC_WLAN_ERROR); 
    sendCommand(cmd); 
    sprintf(cmd, "pagewlan.pwlan2.pic=%d", PIC_WLAN_ERROR_SLAVE); 
    sendCommand(cmd); 
    */
  }
  
  //sendCommand("ref pwlan.pic");
  //sendCommand("ref pagewlan.pwlan2");
}
void setVideo( int mode )
{
  char cmd[64];
  if ( mode == STATUS_BLACK)
  {
    sprintf(cmd, "page0.vavideo.val=%d", PIC_VIDEO_OFF); 
    sendCommand(cmd); 
    sprintf(cmd, "page0.vavideoslave.val=%d", PIC_VIDEO_OFF_SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pvideo.pic=%d", PIC_VIDEO_OFF); 
    sendCommand(cmd); 
    sprintf(cmd, "pagevideo.pvideo2.pic=%d", PIC_VIDEO_OFF_SLAVE); 
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_GREEN)
  {
    sprintf(cmd, "page0.vavideo.val=%d", PIC_VIDEO_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "page0.vavideoslave.val=%d", PIC_VIDEO_ON_SLAVE);  
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pvideo.pic=%d", PIC_VIDEO_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "pagevideo.pvideo2.pic=%d", PIC_VIDEO_ON_SLAVE);  
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_RED)
  {
    sprintf(cmd, "page0.vavideo.val=%d", PIC_VIDEO_ERROR); 
    sendCommand(cmd); 
    sprintf(cmd, "page0.vavideoslave.val=%d", PIC_VIDEO_ERROR_SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pvideo.pic=%d", PIC_VIDEO_ERROR); 
    sendCommand(cmd); 
    sprintf(cmd, "pagevideo.pvideo2.pic=%d", PIC_VIDEO_ERROR_SLAVE); 
    sendCommand(cmd); 
    */
  }
  //sendCommand("ref pvideo.pic");
  //sendCommand("ref pagevideo.pvideo2");

}
void setAlarm( int mode )
{
  char cmd[64];
  if ( mode == STATUS_BLACK)
  {
    sprintf(cmd, "page0.vaalarm.val=%d", PIC_ALARM_OFF); 
    sendCommand(cmd); 
    sprintf(cmd, "page0.vaalarmslave.val=%d", PIC_ALARM_OFF_SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "palarm.pic=%d", PIC_ALARM_OFF); 
    sendCommand(cmd); 
    sprintf(cmd, "pagealarm.palarm2.pic=%d", PIC_ALARM_OFF_SLAVE); 
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_GREEN)
  {
    sprintf(cmd, "page0.vaalarm.val=%d", PIC_ALARM_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "page0.vaalarmslave.val=%d", PIC_ALARM_ON_SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "palarm.pic=%d", PIC_ALARM_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "pagealarm.palarm2.pic=%d", PIC_ALARM_ON_SLAVE);  
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_RED)
  {
    sprintf(cmd, "page0.vaalarm.val=%d", PIC_ALARM_ERROR); 
    sendCommand(cmd); 
    sprintf(cmd, "page0.vaalarmslave.val=%d", PIC_ALARM_ERROR_SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "palarm.pic=%d", PIC_ALARM_ERROR); 
    sendCommand(cmd); 
    sprintf(cmd, "pagealarm.palarm2.pic=%d", PIC_ALARM_ERROR_SLAVE); 
    sendCommand(cmd); 
    */
  }
  //sendCommand("ref palarm.pic");
  //sendCommand("ref pagealarm.palarm2");

}
void setModem( int mode )
{
  char cmd[64];

  sprintf(cmd, "pagemodem.tmodem1.txt=\"%s (%s)\"", modem.oper, modem.mode);
  sendCommand(cmd); 
  sprintf(cmd, "pagemodem.tmodem2.txt=\"%s\"", modem.network );  
  sendCommand(cmd); 
  sprintf(cmd, "pagemodem.tmodem3.txt=\"%s dBm\"", modem.signal);  
  sendCommand(cmd); 

   
  if ( mode == STATUS_BLACK)
  {
    sprintf(cmd, "page0.vamodem.val=%d", PIC_MOBILE_OFF);  
    sendCommand(cmd); 
    sprintf(cmd, "page0.vamodemslave.val=%d", PIC_MOBILE_OFF_SLAVE);  
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pmodem.pic=%d", PIC_MOBILE_OFF);  
    sendCommand(cmd); 
    sprintf(cmd, "pagemodem.pmodem2.pic=%d", PIC_MOBILE_OFF_SLAVE);  
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_GREEN)
  {
    sprintf(cmd, "page0.vamodem.val=%d", PIC_3G_ON); 
    sendCommand(cmd); 
    sprintf(cmd, "page0.vamodemslave.val=%d", PIC_3G__SLAVE); 
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pmodem.pic=%d", PIC_3G_ON); 
    sendCommand(cmd); 
    sprintf(cmd, "pagemodem.pmodem2.pic=%d", PIC_3G__SLAVE); 
    sendCommand(cmd); 
    */
  }
  if ( mode == STATUS_BLUE)
  {
    sprintf(cmd, "page0.vamodem.val=%d", PIC_LTE_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "page0.vamodemslave.val=%d", PIC_LTE_SLAVE);  
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pmodem.pic=%d", PIC_LTE_ON);  
    sendCommand(cmd); 
    sprintf(cmd, "pagemodem.pmodem2.pic=%d", PIC_LTE_SLAVE);  
    sendCommand(cmd);
    */
  }
  if ( mode == STATUS_RED)
  {
    sprintf(cmd, "page0.vamodem.val=%d", PIC_MOBILE_ERROR);  
    sendCommand(cmd); 
    sprintf(cmd, "page0.vamodemslave.val=%d", PIC_MOBILE_ERROR_SLAVE);  
    sendCommand(cmd); 
    /*
    sprintf(cmd, "pmodem.pic=%d", PIC_MOBILE_ERROR);  
    sendCommand(cmd); 
    sprintf(cmd, "pagemodem.pmodem2.pic=%d", PIC_MOBILE_ERROR_SLAVE);  
    sendCommand(cmd); 
    */
  }
  //sendCommand("ref pmodem.pic");
  //sendCommand("ref pagemodem.pmodem2");
  
}
void updateImageStatus()
{ 
    if ( netz[IND_VPN].updown == INT_UP && netz[IND_VPN].ipadr == INT_IP)
        imagestatus[IMG_VPN].status = STATUS_GREEN;
    else
        imagestatus[IMG_VPN].status = STATUS_RED;

    if ( isAlarm == 1)
        imagestatus[IMG_ALARM].status = STATUS_GREEN;
    else
        imagestatus[IMG_ALARM].status = STATUS_RED;

    if ( isVideo == 0)
        imagestatus[IMG_VIDEO].status = STATUS_GREEN;
    else
        imagestatus[IMG_VIDEO].status = STATUS_RED;

    if ( netz[IND_LAN].updown == INT_UP && netz[IND_LAN].ipadr == INT_IP)
        imagestatus[IMG_LAN].status = STATUS_GREEN;
    else
        imagestatus[IMG_LAN].status = STATUS_RED;



    if ( netz[IND_WAN].updown == INT_UP && netz[IND_WAN].ipadr == INT_IP)
        imagestatus[IMG_WAN].status = STATUS_GREEN;
    else
        imagestatus[IMG_WAN].status = STATUS_BLACK;

    if ( netz[IND_MODEM].updown == INT_UP && netz[IND_MODEM].ipadr == INT_IP)
        if ( modem.lte == 2)
            imagestatus[IMG_MODEM].status = STATUS_BLUE;
        else
            imagestatus[IMG_MODEM].status = STATUS_GREEN;
    else
        imagestatus[IMG_MODEM].status = STATUS_BLACK;

    if ( netz[IND_WLAN].updown == INT_UP && netz[IND_WLAN].ipadr == INT_IP)
        imagestatus[IMG_WLAN].status = STATUS_GREEN;
    else
        imagestatus[IMG_WLAN].status = STATUS_BLACK;

    imagestatus[IMG_INFO].status = STATUS_GREEN;



}
void updateCounters()
{
  char cmd[128];
printf( "updateCounters\n");

  //printf( "IND_VPN:%s\n", netz[IND_VPN].ipstr);
  sprintf(cmd, "pagevpn.tvpnip.txt=\"%s\"", netz[IND_VPN].ipstr); 
  sendCommand(cmd); 
  //printf( "WAN:%s\n", netz[IND_WAN].ipstr);
  sprintf(cmd, "pagewan.twanip.txt=\"%s\"", netz[IND_WAN].ipstr); 
  //printf( "WAN==>%s\n", netz[IND_WAN].ipstr);
  sendCommand(cmd); 
  //printf( "WAN===>%s\n", netz[IND_WAN].ipstr);
  sprintf(cmd, "pagelan.tlanip.txt=\"%s\"", netz[IND_LAN].ipstr); 
  sendCommand(cmd); 

  //sprintf(cmd, "pagevpn.tvpnreceive.txt=\"%s\"", netz[IND_VPN].rxstr);
  //sendCommand(cmd);
  sprintf(cmd, "pagewan.twanreceive.txt=\"%s\"", netz[IND_WAN].rxstr); 
  sendCommand(cmd); 
  sprintf(cmd, "pagelan.tlanreceive.txt=\"%s\"", netz[IND_LAN].rxstr); 
  sendCommand(cmd); 
  
  //sprintf(cmd, "pagevpn.tvpnsended.txt=\"%s\"", netz[IND_VPN].txstr);
  //sendCommand(cmd);
  sprintf(cmd, "pagewan.twansended.txt=\"%s\"", netz[IND_WAN].txstr); 
  sendCommand(cmd); 
  sprintf(cmd, "pagelan.tlansended.txt=\"%s\"", netz[IND_LAN].txstr); 
  sendCommand(cmd); 

  if ( video[0].port != 0)
  {
    sprintf(cmd, "pagevideo.tvideo1.txt=\"%s %s\"", video[0].name, video[0].statusstr);
    sendCommand(cmd);
  }
  if ( video[1].port != 0)
  {
    sprintf(cmd, "pagevideo.tvideo2.txt=\"%s %s\"",  video[1].name, video[1].statusstr);
    sendCommand(cmd);
  }
  if ( video[2].port != 0)
  {
    sprintf(cmd, "pagevideo.tvideo1.txt=\"%s %s\"",  video[2].name, video[2].statusstr);
    sendCommand(cmd);
  }


 printf( "updateCounters OK\n");

}

int checkVideo( char *ip, int port)
{

	int err, net;
	struct hostent *host;
	struct sockaddr_in sa;
	struct timeval timeout;      
	
        timeout.tv_sec = 0;
        timeout.tv_usec = 500000;
        
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	sa.sin_addr.s_addr = inet_addr(ip);
	net = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (setsockopt (net, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
                sizeof(timeout)) < 0)
        printf("setsockopt failed\n");

    if (setsockopt (net, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,
                sizeof(timeout)) < 0)
        printf("setsockopt failed\n");

	err = connect(net, (struct sockaddr *)&sa, sizeof(sa));

	if(err >= 0)
	{ 
          return PORT_OK;
	}
	else 
	{ 
          return PORT_CLOSE;
	}
        return PORT_CLOSE;

}
static void
print_attr_val(const struct switch_attr *attr, const struct switch_val *val)
{
	int i;

	switch (attr->type) {
	case SWITCH_TYPE_INT:
		//printf("%d", val->value.i);
		break;
	case SWITCH_TYPE_STRING:
		printf("%s", val->value.s);
				  if ( strstr( val->value.s, "link:up") != NULL)
				    isAlarm = 1; //printf("UP");
				  if ( strstr( val->value.s, "link:down") != NULL)
				    isAlarm = 0; //printf("DOWN");
				    
		break;
	case SWITCH_TYPE_PORTS:
	/*
		for(i = 0; i < val->len; i++) {
			printf("%d%s ",
				val->value.ports[i].id,
				(val->value.ports[i].flags &
				 SWLIB_PORT_FLAG_TAGGED) ? "t" : "");
		}
		* */
		break;
	default:
		printf("?unknown-type?");
	}
}

static void
show_attrs(struct switch_dev *dev, struct switch_attr *attr, struct switch_val *val)
{
	while (attr) {
		if (attr->type != SWITCH_TYPE_NOVAL) {
			printf("\t%s: ", attr->name);
			if (swlib_get_attr(dev, attr, val) < 0)
				printf("???");
			else
			{
				//if ( strcmp( attr->name, "link") == 0)
/*				  if ( strstr( val->value.s, "link:up") != NULL)
				    printf("UP");
				  if ( strstr( val->value.s, "link:down") != NULL)
				    printf("DOWN");
*/				    
				  print_attr_val(attr, val);
			}	
			putchar('\n');
		}
		attr = attr->next;
	}
}

static void
show_port(struct switch_dev *dev, int port)
{
	struct switch_val val;

	printf("Port %d:\n", port);
	val.port_vlan = port;
	show_attrs(dev, dev->port_ops, &val);
}
int checkAlarm()
{
	struct switch_dev *dev;
	char *cdev = "eth0";
	int i;
	
	dev = swlib_connect(cdev);
	if (!dev) {
		fprintf(stderr, "Failed to connect to the switch. Use the \"list\" command to see which switches are available.\n");
		return 1;
	}
    swlib_scan(dev);

//	for (i=0; i < dev->ports; i++)
//		show_port(dev, i);

	show_port(dev, 4);
	swlib_free_all(dev);
	
	return 0;
}

int gsm()
{ 

    char s[3][48] = {"/bin/sh", "-c", "gsmctl -Uoqtg > /tmp/log.txt"};
    char *a[4];

	int pid, status;
	int i;
    
    for (i = 0; i < 3; i++)
        a[i] = s[i];
    a[i] = NULL;	
	
	pid = fork();
	if (pid == 0) 
	{
		//printf("%s", progname);
		execvp("/bin/sh", a);
		return EXIT_FAILURE; // Never get there normally
	} else {
		if (wait(&status) == -1) {
			perror("wait");
			return EXIT_FAILURE;
		}
		if (WIFEXITED(status))
			return 0;
		
//			printf("Child terminated normally with exit code %i\n", WEXITSTATUS(status));
		if (WIFSIGNALED(status))
			return -1;
			//printf("Child was terminated by a signal #%i\n", WTERMSIG(status));
		if (WCOREDUMP(status))
			return -1;
			//printf("Child dumped core\n");
		if (WIFSTOPPED(status))
			return -1;
			//printf("Child was stopped by a signal #%i\n", WSTOPSIG(status));
	}
	return 0; //EXIT_SUCCESS;
}
// -Uoqtg

int main(int argc, char **argv) 
{
  char buf[128];
  int i;

  isAlarm= 0;
  
  
//  return 0;
  loadConfig();
  for ( i=0; i< 8; i++)
  {
      imagestatus[i].status = 0;
      imagestatus[i].oldstatus = 0;
  }

  for ( i=0; i< 5; i++)
  {
        sprintf(netz[i].rxstr," ");
        sprintf(netz[i].txstr," ");
	sprintf(netz[i].ipstr,"IP:");
  }
  //printf( "WAN:%s\n", netz[IND_WAN].ipstr);
  fd = open_port();
  configure_port(fd);
  sendCommand("");
  sendCommand("bkcmd=1");
  int ret1 = read(fd, buf, 128 );
  while (ret1 > 0)
    ret1 = read(fd, buf, 128 );
  

  int cnt= 10;
  while (1)
  { 
    isVideo = getVideoStatus();
    checkAlarm();
    checkInterfaces();
	  
	  if ( (cnt % 10) == 0)
	  { 
		getIp("br-lan");  // lan
		getIp("tap0");  // vpn
		getIp("eth1");  // alarm
		getIp("eth2");  // modem
		getIp("wlan0");  // wlan
	  }
	  
	  ifMetric();
      if ( gsm()== 0) 
         parse_gsm();
      updateCounters();
      updateImageStatus();
      updateStatus();

      sendCommand("page0.tm1.en=0");
      sendCommand("page0.tm1.tim=20000");

        sleep(1);
        if ( ++cnt > 99) cnt= 0;
	
  }
  
  /*

  sendCommand("pagevpn.tvpnip.txt=\"000.---.---.---\"");
*/
  close(fd);
  
  return 0;
}




