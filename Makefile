#=====================================
# OpenWrt Makefile
#=====================================

include $(TOPDIR)/rules.mk

PKG_NAME:=nextion
PKG_VERSION:=1.1.1
PKG_RELEASE:=1

PKG_BUILD_DIR:= $(BUILD_DIR)/$(PKG_NAME)
#PKG_BUILD_DEPENDS:=libpcap


include $(INCLUDE_DIR)/package.mk


define Package/nextion
	SECTION:=utils
	CATEGORY:=Utilities
	TITLE:=Nextion display utility
#	EPENDS:=+libpcap +libcurl 
	MAINTAINER:=Sergey Mizun <serg.mizun@gmail.com>
endef

define Package/nextion/description
	Nextion display utility
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)/
endef

#TARGET_LDFLAGS += $(STAGING_DIR)/usr/lib/libpcap.a  $(STAGING_DIR)/usr/lib/libcurl.a

define Build/Compile
	$(TARGET_CC) $(TARGET_CFLAGS) -c -o $(PKG_BUILD_DIR)/nextion.o $(PKG_BUILD_DIR)/nextion.c
#	$(TARGET_CC) $(TARGET_CFLAGS) -c -o $(PKG_BUILD_DIR)/radiotap.o $(PKG_BUILD_DIR)/radiotap.c
	$(TARGET_CC) $(TARGET_LDFLAGS) -o $(PKG_BUILD_DIR)/nextion $(PKG_BUILD_DIR)/nextion.o 
endef

define Package/nextion/install
	$(INSTALL_DIR) $(1)/bin
#	$(INSTALL_DIR) $(1)/etc/init.d
#	$(INSTALL_DIR) $(1)/etc/config
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/nextion $(1)/
#	$(INSTALL_BIN) ./etc/init.d/rssi $(1)/etc/init.d/nextion
#	$(INSTALL_BIN) ./etc/config/rssi.conf $(1)/etc/config/rssi.conf
endef

$(eval $(call BuildPackage,nextion))	
	

